import numpy as np
from random import shuffle
from past.builtins import xrange

def to_binary(y, num_classes):
    return np.eye(num_classes)[y]


def softmax(x):
    exps = np.exp(x)
    return exps/np.expand_dims(exps.sum(axis=-1), axis=-1)


def softmax_loss_naive(W, X, y, reg):
  """
  Softmax loss function, naive implementation (with loops)

  Inputs have dimension D, there are C classes, and we operate on minibatches
  of N examples.

  Inputs:
  - W: A numpy array of shape (D, C) containing weights.
  - X: A numpy array of shape (N, D) containing a minibatch of data.
  - y: A numpy array of shape (N,) containing training labels; y[i] = c means
    that X[i] has label c, where 0 <= c < C.
  - reg: (float) regularization strength

  Returns a tuple of:
  - loss as single float
  - gradient with respect to weights W; an array of same shape as W
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)

  #############################################################################
  # TODO: Compute the softmax loss and its gradient using explicit loops.     #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  num_classes = W.shape[1]
  for x, yv in zip(X, y):
    # Wx = Z
    # softmax(Z) = score
    # loss = log loss
    Z = x.dot(W)
    scores = softmax(Z)
    corect_class_score = scores[yv]
    loss += np.log(corect_class_score)
    
    # https://math.stackexchange.com/questions/945871/derivative-of-softmax-loss-function
    dZ = scores - to_binary(yv, num_classes)
    dW += x.reshape(-1,1).dot(dZ.reshape(1,-1))
    
  loss = loss / -len(X)
  dW = dW / len(X)

  loss += reg * np.sum(W * W)
  dW += 2 * reg * W
  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW


def softmax_loss_vectorized(W, X, y, reg):
  """
  Softmax loss function, vectorized version.

  Inputs and outputs are the same as softmax_loss_naive.
  """
  # Initialize the loss and gradient to zero.
  loss = 0.0
  dW = np.zeros_like(W)

  #############################################################################
  # TODO: Compute the softmax loss and its gradient using no explicit loops.  #
  # Store the loss in loss and the gradient in dW. If you are not careful     #
  # here, it is easy to run into numeric instability. Don't forget the        #
  # regularization!                                                           #
  #############################################################################
  num_classes = W.shape[1]
  Z = X.dot(W)
  scores = softmax(Z)
  corect_class_score = scores[np.arange(len(X)), y]
  loss = np.log(corect_class_score).sum()

  dZ = scores - to_binary(y, num_classes)
  dW = X.T.dot(dZ)

  loss = loss / -len(X)
  dW = dW / len(X)

  loss += reg * np.sum(W * W)
  dW += 2 * reg * W
  #############################################################################
  #                          END OF YOUR CODE                                 #
  #############################################################################

  return loss, dW

